package com.company.reservation;

import java.util.Date;
import com.company.reservation.User;
import java.util.concurrent.TimeUnit;

public class Room {

    private final int roomNumber;
    private final int bedsNumber;
    private boolean isReserved;
    private Date reservationStarts;
    private Date reservationEnds;
    private User reserverUser;
    private double price;


    public Room(int bedsNumber, boolean isReserved, int roomNumber, double price){
        this.bedsNumber = bedsNumber;
        this.isReserved = isReserved;
        this.roomNumber = roomNumber;
        this.price = price;
    }

    public boolean isReserved(){
        return this.isReserved;
    }

    public void reserve(User reserver, Date reservationStarts, Date reservationEnds){
        if(!this.isReserved){
            this.reserverUser = reserver;
            this.isReserved = true;
            this.reservationEnds = reservationEnds;
            this.reservationStarts = reservationStarts;
            int days = TimeUnit.DAYS.convert((reservationStarts.getTime() - reservationEnds.getTime()), TimeUnit.MILLISECONDS)
            reserver.addNewReservation(new Reservation(this, reservationStarts, reservationEnds, reserver, price * days));
        }
    }

    public int getRoomNumber(){
        return roomNumber;
    }

    public int numbersOfBed(){
        return this.bedsNumber;
    }


    @Override
    public String toString(){
    
        return "The room " + roomNumber + " has " + bedsNumber + " bed for " + price  + "$ per night.";
    }
}