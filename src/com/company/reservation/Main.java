package com.company.reservation;

import com.company.reservation.Booking;
import java.util.Scanner;


public class Main{

    public static void main(String[] args){
        Booking booking = new Booking();
        User user = new User("Venyige", "Balázs");
        booking.login(user);
        /* some business logic to use the application*/
    }
}