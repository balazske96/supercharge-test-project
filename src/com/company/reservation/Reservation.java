package com.company.reservation;

import java.util.Date;
import java.util.UUID;
import com.company.reservation.Room;
import com.company.reservation.User;

public class Reservation {


    public final String id = UUID.randomUUID().toString();
    public Room room;
    public Date startingDate;
    public Date endingDate;
    public User user;
    public boolean isCanceled;
    public double price;


    public Reservation(Room room, Date startingDate, Date endingDate, User user, double price){
        this.user = user;
        this.endingDate = endingDate;
        this.startingDate = startingDate;
        this.room = room;
        this.price = price;
    }

    public void cancel(){
        this.isCanceled = true;
    }


    @Override
    public String toString(){
        return "This reservation was made by " + user + " from " + startingDate + " to " + endingDate + " for the price of " + price;
    }


}