package com.company.reservation;

import java.util.*;
import com.company.reservation.Room;
import java.text.SimpleDateFormat;
import com.company.reservation.User;

public class Booking {
    

    private List<Room> Rooms = new ArrayList<Room>();
    private User user;


    //Static room construction
    public Booking(){
        Room room1 = new Room(2, false, 1, 200.0);
        Room room2 = new Room(2, false, 2, 200.0);
        Room room3 = new Room(3, false, 3, 300.0);
        Room room4 = new Room(4, false, 4, 400.0);
        Room room5 = new Room(5, false, 5, 500.0);
        Rooms.add(room1);
        Rooms.add(room2);
        Rooms.add(room3);
        Rooms.add(room4);
        Rooms.add(room5);
    }

    public void getOpenRooms(){
        System.out.println("You can choose from the rooms below:");
        for(int i = 0; i < Rooms.size(); i++){
            Room room = Rooms.get(i);
            if(!room.isReserved()){
                System.out.println(room.toString());
            }
        }
    }

    public void book(String bookDetails) throws Exception {
        String[] details = bookDetails.split(",");
        int roomNumber = Integer.parseInt(details[0]);
        Date start = new SimpleDateFormat("dd/MM/yyyy").parse(details[1]);
        Date end = new SimpleDateFormat("dd/MM/yyyy").parse(details[2]);
        for(Room room : Rooms){
            if(room.getRoomNumber() == roomNumber){
                room.reserve(this.user, start, end);
            }
        }
    }

    public void cancelReservation(String cancelationDetails){
        //TODO
    }
    
    public void login(User user){
        this.user = user;
    }

    public void logout(){
        this.user = null;
    }

    

}