package com.company.reservation;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import com.company.reservation.Reservation;

public class User {


    private String firstName;
    private String lastName;

    List<Reservation> previousReservations = new ArrayList<Reservation>();



    public User(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public void addNewReservation(Reservation reservation){
        previousReservations.add(reservation);
    }

    public void printBookingHistory(){
        for(Reservation reservation : previousReservations){
            System.out.println(reservation);
        }
    }

    public List<Reservation> getReservationHistory(){
        return previousReservations;
    }

    public List<Reservation> filterReservationHistoryByStartingDate(Date startingDate){
        return previousReservations.stream()
            .filter(reservation -> reservation.startingDate.equals(startingDate))
            .collect(Collectors.toList());
    }

    public List<Reservation> filterReservationHistoryByEndingDate(Date endingDate){
        return previousReservations.stream()
            .filter(reservation -> reservation.endingDate.equals(endingDate))
            .collect(Collectors.toList());
    }

    public List<Reservation> filterReservationHistoryByDays(int numberOFDay){
        return previousReservations.stream()
            .filter(reservation -> TimeUnit.DAYS.convert((reservation.endingDate.getTime() - reservation.startingDate.getTime()), TimeUnit.MILLISECONDS) == numberOFDay)
            .collect(Collectors.toList());
    }

    public List<Reservation> filterReservationHistoryByPrice(double price){
        return previousReservations.stream()
            .filter(reservation -> reservation.price == price)
            .collect(Collectors.toList());
    }

}